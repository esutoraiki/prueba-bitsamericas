(function ($) {
    "use strict";

    $.extend({
        customSelect: function () {
            var attr,
                class_1,
                value_select,
                select_1,
                select_2,
                select_3,
                select_4;

            function process() {
                $("body").on("click", function () {
                    select_1.removeClass(class_1);
                });

                select_1.on("click", function (e) {
                    e.stopPropagation();
                    select_1.toggleClass(class_1);
                });

                select_3.on("click", function (e) {
                    e.stopPropagation();
                    $(attr.container + " .custom_select li." + class_1)
                        .removeClass(class_1);

                    $(this).each(function () {
                        value_select = $(this).data("value");

                        //Assignment of values
                        select_2.attr("data-value", value_select)
                            .text(value_select);
                        $(this).addClass(class_1);

                        //remove class
                        select_1.removeClass(class_1);

                        //Change value select
                        select_4.val(value_select);
                    });
                });
            }

            return {
                init: function (attr_e) {
                    attr = attr_e || {};

                    class_1 =  attr.class_ok,
                    select_1 = $(attr.container),
                    select_2 = $(attr.container + " .input_custom_select"),
                    select_3 = $(attr.container + " .custom_select li"),
                    select_4 = $(attr.container + " select[name=adult]");

                    process();
                }
            }
        }
    });
}(jQuery));
