class Slider {
    constructor(attr = {}) {
        this.id_slider = attr.id_slider || "#slider";
        this.class_item = attr.class_item || ".slider_item";
        this.class_ok = attr.class_ok || "active";
        this.container = attr.container || ".container_slider";
        this.pointer = 1;
        this.num = $(this.class_item).length;
        this.time = attr.time || 8000;
        this.set_time = null;
        this.startx = null;
        this.dist = null;
    }

    init() {
        this.mousemove(); //Lock slider by moving the mouse
        this.touch(); //Events touch
        this.auto(); //Automatic movement of the slider
    }

    stop_auto() {
        window.clearTimeout(this.set_time);
    }

    change_pointer(direction = false) {
		if (direction) {
            this.pointer--;

            if (this.pointer <= 0) {
                this.pointer = this.num;
            }
        } else {
            this.pointer++;

            if (this.pointer > this.num) {
                this.pointer = 1;
            }
        }
    }

    change_active(direction = false) {
        let i;

        this.stop_auto();
        this.change_pointer(direction);
        for (i = 1; i <= this.num; i++) {
            $(this.id_slider + " " + this.class_item)
                .removeClass(this.class_ok);
        }

        $(this.id_slider + "_" + this.pointer).addClass(this.class_ok);

        this.auto();
    }

    auto() {
        //Without use window.setinterval by efficiency
        this.set_time = window.setTimeout(function () {
            this.change_active();
        }.bind(this), this.time);
    }

    mousemove() {
        $(this.id_slider).on("mousemove", function() {
            this.stop_auto();
            this.auto();
        }.bind(this));
    }

    touch() {
        let container = $(this.container);

        container.on("touchstart", function (e) {
            let touchobj = e.changedTouches[0];
            this.startx = parseInt(touchobj.clientX);
        }.bind(this));

        container.on("touchmove", function (e) {
            let touchobj = e.changedTouches[0];
            this.dist = parseInt(touchobj.clientX) - this.startx;
        }.bind(this));

        container.on("touchend", function (e) {
            if (this.dist > 50) {
                this.change_active();
            } else if (this.dist < -50) {
                this.change_active(true);
            }
        }.bind(this));
    }
}
