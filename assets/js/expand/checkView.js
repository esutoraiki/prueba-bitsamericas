/*global document, window, console, jQuery, navigator */
/*jslint maxlen: 150, plusplus: true, unparam: true*/

(function ($) {
    "use strict";

    /**
     * Checks if an element is visible
     */
    $.fn.check_view = function (attr_e) {
        var attr    = attr_e || {},

            variation_high = attr.variation_high || 0,
            getVersion     = attr.getVersion || false,
            success        = attr.success || function () { return undefined; },
            nsucces        = attr.no_success || function () { return undefined; },
            version        = 20160928;

        if (getVersion) {
            console.log(version);
        }

        $(window).on("load scroll resize", function () {
            var wheight = $(window).height(),
                wtop    = $(window).scrollTop(),
                wbottom = (wheight + wtop);

            $.each($(this), function () {
                var element = $(this),
                    eheight = element.outerHeight(),
                    etop    = element.offset().top + variation_high,
                    ebottom = (eheight + etop);

                if ((ebottom >= wtop) && (etop <= wbottom) && (ebottom !== 0 && etop !== 0)) {
                    success($(this));
                } else {
                    nsucces($(this));
                }
            });
        }.bind($(this))).trigger("scroll");
    };
}(jQuery));
