(function ($) {
    "use strict";

    var NSFB = {};

    NSFB = (function () {
        /* Private */
        /* Public */
        return {
            slider: function () {
                let main_slider = new Slider();
                main_slider.init();
            },

            click_div_select: function () {
                var select_1 = new $.customSelect(),
                    select_2 = new $.customSelect();

                select_1.init({
                    class_ok: "active",
                    container: ".group_custom_select"
                });

                select_2.init({
                    class_ok: "active",
                    container: ".group_custom_select_2"
                });
            },

            book_now: function () {
                $(".button_book_now").on("click", function () {
                    $(".book_now").removeClass("close");
                    $(".book_now").addClass("open");
                });

                $(".book_now .close").on("click", function () {
                    $(".book_now").removeClass("open");
                    $(".book_now").addClass("close");
                })
            },

            animations: function () {
                $(".services .card_1").check_view({
                    success: function () {
                        $(".services .card_1").addClass("animated bounce");
                    }
                });

                $(".services .card_2").check_view({
                    success: function () {
                        $(".services .card_2").addClass("animated bounce");
                    }
                });

                $(".services .card_3").check_view({
                    success: function () {
                        $(".services .card_3").addClass("animated bounce");
                    }
                });

                $(".testimonials .card_1").check_view({
                    success: function () {
                        $(".testimonials .card_1").addClass("animated fadeInLeft");
                    }
                });

                $(".testimonials .card_2").check_view({
                    success: function () {
                        $(".testimonials .card_2").addClass("animated fadeInRight");
                    }
                });

                $(".logo_footer").check_view({
                    success: function () {
                        $(".logo_footer").addClass("animated rubberBand");
                    }
                });
            }
        };
    }());

    $(window).on("load", function () {
        if ($("#slider").length !== 0) {
            NSFB.slider();
        }

        NSFB.click_div_select();
        NSFB.book_now();
        NSFB.animations();
    });
}(jQuery));
